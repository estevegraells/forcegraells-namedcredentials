# Readme
Este repo contiene el proyecto Java (Spring boot) y el código Apex, correspondiente a la entrada del blog ForceGraells.com donde explico las Named Credetials en Salesforce ( https://bitbucket.org/estevegraells/forcegraells-namedcredentials)

## Parte Java
El código Java está basado en Maven, y ofrece 2 Web Services, uno autenticado y el otro sin autenticación. La autenticación se encuentra en el código de la parte de Security, para modificar las credenciales y roles como se desee.
En mi caso desplegué la aplicación directamente en Heroku, aunque estoy seguro que siendo una App Spring Boot, puede ser desplegada con facilidad en cualquier Cloud Público.

## Parte Salesforce
Consiste en:

1. Página VisualForce para realizar invoacar las funciones del Controller

1. Controller como intermediario entre la vista y el modelo (aunque puede ser discutible su necesidad en un proyecto tan senzillo)

1. El modelo que realiza las llamdas y donde se utilizan las Named Credentials.

Espero que sea de ayuda. 