/*
 * Esteve Graells
 * */

package graells.echoservice;

import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
public class EchoController {

    @RequestMapping("/HolaConAuth")
    public Map<String,String> holaAuth(
                @RequestHeader HttpHeaders cabeceras) {

        Map<String,String> r = new HashMap<>();
        r.put("Recibida petición", "");
        r.put("time", new Date().toString());

        for(String str : cabeceras.keySet()){
            r.put(str, cabeceras.getFirst(str));
        }

        System.out.println(r);

        /*The object data will be written directly to the HTTP response as JSON.
         * The respuesta object must be converted to JSON. because Jackson 2,
         * Spring’s MappingJackson2HttpMessageConverter is automatically chosen to convert to JSON.
         * */

        return r;
    }

    @RequestMapping("/HolaSinAuth")
    public Map<String,String> holaSinAuth(@RequestHeader HttpHeaders cabeceras) {

        Map<String,String> r = new HashMap<>();
        r.put("Recibida petición", "");
        r.put("time", new Date().toString());

        for(String str : cabeceras.keySet()){
            r.put(str, cabeceras.getFirst(str));
        }

        System.out.println(r);

        /*The object data will be written directly to the HTTP response as JSON.
         * The respuesta object must be converted to JSON. because Jackson 2,
         * Spring’s MappingJackson2HttpMessageConverter is automatically chosen to convert to JSON.
         * */

        return r;
    }
}
